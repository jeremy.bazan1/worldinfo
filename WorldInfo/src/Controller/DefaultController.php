<?php


namespace App\Controller;

use App\Entity\Task;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class DefaultController extends AbstractController
{
    public function new(Request $request)
    {

        /**
        * @Route("/",methods="GET")
        */

        $task = new Task();

        $form = $this->createFormBuilder($task)

            ->add('task', TextType::class)
            ->add('name', TextType::class)
            ->add('numberOffice' , IntegerType::class)
            ->add('problem', ChoiceType::class)
            ->add('numberComputer' , IntegerType::class)
            ->add('description' , TextareaType::class)
            ->add('validation', SubmitType::class, ['label' => 'Submite me'])
            ->getForm();

        return $this->render('default/intervention.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}