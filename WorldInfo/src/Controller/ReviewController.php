<?php


namespace App\Controller;

use App\Entity\Task;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class ReviewController extends AbstractController
{

        /**
        * @Route("/",methods="POST")
        */

        public function findAll(UserRepository $repo)
        {
            $serializer = $this->get('jms_serializer');
            $repo = $this->getdoctrine()->getRepository(User::class);
            $user = $repo->findAll();
            $json = $serializer->serialize($user, "json");

            return JsonResponse::fromJsonString($json);
        }
}